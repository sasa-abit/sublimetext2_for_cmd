﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SublimeText2_for_CmdLine
{
    public partial class Form1 : Form
    {
        const string sublime_dir = "C:\\Program Files\\Sublime Text 2";

        public Form1()
        {
            InitializeComponent();
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            string path = System.Environment.GetEnvironmentVariable("Path", 
                System.EnvironmentVariableTarget.Machine);

            if (path.IndexOf(sublime_dir) >= 0)
            {
                //MessageBox.Show("既にSublime Text 2はPathに通ってますにゃ");
            }
            else
            {
                //ここでPathに追加しる
                string new_path = path + ";" + sublime_dir;
                //MessageBox.Show(path);
                System.Environment.SetEnvironmentVariable("Path", new_path, System.EnvironmentVariableTarget.Machine);
            }
            System.Environment.SetEnvironmentVariable("SUBLIME_HOME", sublime_dir + "\\", System.EnvironmentVariableTarget.Machine);

            MessageBox.Show("新しくDOS窓を開くと、そこからsublime_textを実行できます。", "設定おｋ",MessageBoxButtons.OK);
        }
    }
}
